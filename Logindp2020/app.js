$('#formlogin').submit(function (error) {
    error.preventDefault();
    var usuario = $.trim($("#usuario").val());
    var password = $.trim($("#password").val());
/*En esta parte les paso las condionales  al "if" usando Sweetalet2(swal.fir) y en caso
de que el usuario no ingrese ningun dato que no haga nada "return false"*/
    if (usuario.length == "" || password == "") {
        swal.fire({
            type:"warning",
            title:"Ingrese Usuario y/o Contraseña",
        });
        return false;
    }else{
        $.ajax({
            url:"bd/login.php",
            type:"POST",
            datatype:"json",
            data: {usuario:usuario, password:password},
            success:function(data){
                if(data == "null"){
                    swal.fire({
                        type:"error",
                        title:"Usuairo y/o Contraseña es incorrecta",
                        confirmButtonColor:"#f3970c",
                    });
                }else{
                    swal.fire({
                        type:"success",
                        title:"Conexion exitosa",
                        confirmButtonColor:"#0bf145",
                        confirmButtonText:"Ingresar"
                    }).then((result) =>{
                        if(result.value){
                            window.location.href = "vistas/pagina_inicio.php";
                        }
                    })
                }
            }
        })
    }

});

//Aqui les dejo los usuarios y contraseñas://
//addmin:12345//
//demo:demo//
